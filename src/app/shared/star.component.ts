import { Component, OnChanges } from '@angular/core' ;

@Component({
    selector: 'pm-star',
    templateUrl: './star.component.html',
    styleUrls: ['./star.component.css']
})

export class StarComponent implements OnChanges {
    // ma vision du onchange ici , c'est qu'il faudra ajuster le nbre d'étoiles
    //en fonction des chiffres ( chiffre qui seront afficher en InInit j'imagine)
    rating: number= 4;  // nbre en dur pour le moment
    starWidth: number; // pour la gestion de la convertion/affichage voir ▼

    ngOnChanges(): void {
        this.starWidth = this.rating * 75 / 5; 
        // permet d'ajuster la taille de l'affichage, smart
    }
}