import { Component, OnInit } from '@angular/core';
import { IProduct } from './products';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  ProductListTitle: string = 'Product List !';
  imageWidth: number = 50;
  imageMargin: number = 2;
  showImage: boolean = false;
  // getter and setter pour le nouveau system de filtre ▼
  // le getter pour récupérer la valeur, le setter pour la modifier
  _listFilter: string;
  get listFilter(): string {
    return this._listFilter; // 
  }
  set listFilter(value:string) {
    this._listFilter = value;
    this.filteredProducts = this.listFilter ? this.performFilter(this.listFilter) : this.products;
  }

  filteredProducts: IProduct[];
  products: IProduct[]= [
    {
      "productId": 2,
      "productName": "Garden Cart",
      "productCode": "GDN-0023",
      "releaseDate": "March 18, 2019",
      "description": "15 gallon capacity rolling garden",
      "price": 32.99,
      "starRating": 4.2,
      "imageUrl": "assets/images/garden_cart.png"
    },
    {
      "productId": 5,
      "productName": "Hammer",
      "productCode": "TBX-0048",
      "releaseDate": "May 12, 2019",
      "description": "Curved claw steel hammer",
      "price": 8.9,
      "starRating": 4.8,
      "imageUrl": "assets/images/hammer.png"
    }
  ];

  // constructeur  - apparment pour mettre les valeurs par défaut 
  constructor() {
    this.filteredProducts = this.products;
    this.listFilter = '' ; 
  }

  // création de la méthode qui permet de lire la liste avant de la filtrer
  performFilter(filterBy: string): IProduct[] {
    filterBy = filterBy.toLocaleLowerCase(); // interet ? comparer Apple et apple 
    return this.products.filter((product: IProduct) => // nouvelle array this.products.filter 
            product.productName.toLocaleLowerCase().indexOf(filterBy) !== -1); // for each line, all in lowercase
  }

  toggleImage(): void {
    this.showImage = !this.showImage;
  }
  ngOnInit(): void {
    console.log('In onInit');
  }
}
